import urwid
import sys
from tiramisu_text_ui import TiramisuTui, get_palette
from examples.AmonEcole.amonecole import get_config


class AmonEcoleUI:

    def __init__(self,
                 config,
                 root):
        footer_text = [
            ('title', "Example Data Browser"), "    ",
            ('key', "UP"), ",", ('key', "DOWN"), ",",
            ('key', "PAGE UP"), ",", ('key', "PAGE DOWN"),
            "  ",
            ('key', "+"), ",",
            ('key', "-"), "  ",
            ('key', "LEFT"), "  ",
            ('key', "HOME"), "  ",
            ('key', "END"), "  ",
            ('key', "F8"), " quit"
            ]

        palette = get_palette()
        palette.extend([('body', 'black', 'light gray'),
                        ('head', 'yellow', 'black', 'standout'),
                        ('foot', 'light gray', 'black')])
        tui = TiramisuTui(config, root)
        tui.offset_rows = 1
        header = urwid.Text('tiramisu browser')
        footer = urwid.AttrWrap(urwid.Text(footer_text),
                                'foot')
        view = urwid.Frame(
            urwid.AttrWrap(tui, 'body'),
            header=urwid.AttrWrap(header, 'head'),
            footer=footer)
        tui.set_frame(view)

        # Run the program
        loop = urwid.MainLoop(view,
                              palette,
                              unhandled_input=self.unhandled_input)
        loop.run()

    def unhandled_input(self, key):
        if key in ('f8',):
            raise urwid.ExitMainLoop()


def main():
    config = get_config()
    config.property.read_write()
    config.property.add('demoting_error_warning')
    # config.property.add('expert')
    # config.property.add('normal')
    config.permissive.add('expert')
    config.permissive.add('normal')
    AmonEcoleUI(config, root='creole')
    exportation = config.value.exportation()
    for idx, path in enumerate(exportation[0]):
        if exportation[3][idx] != 'forced':
            print(path, config.option(path).value.get())


if __name__ == "__main__":
    main()
