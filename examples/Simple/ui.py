# Copyright (C) 2018 Team tiramisu (see AUTHORS for all contributors)
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/usr/bin/env python
#


import urwid
from tiramisu import Config, OptionDescription, Option, StrOption, IntOption, \
        BoolOption, ChoiceOption, Params, ParamOption
from tiramisu_text_ui import TiramisuTui


def return_value(value):
    return value


class SimpleUI:
    palette = [
        ('body', 'black', 'light gray'),
        ('flagged', 'black', 'dark green', ('bold', 'underline')),
        ('focus', 'light gray', 'dark cyan', 'standout'),
        ('flagged focus', 'yellow', 'dark cyan',
         ('bold', 'standout', 'underline')),
        ('head', 'yellow', 'black', 'standout'),
        ('foot', 'light gray', 'black'),
        ('key', 'light cyan', 'black', 'underline'),
        ('title', 'white', 'black', 'bold'),
        ('expandico', 'black', 'light gray', 'standout'),
        ('expandicofc', 'black', 'dark cyan', 'bold'),
        ('unexpandico', 'black', 'light gray', 'standout'),
        ('unexpandicofc', 'black', 'dark cyan', 'bold'),
        ('flag', 'dark gray', 'light gray'),
        ('error', 'dark red', 'light gray'),
        ('editfc', 'white', 'dark green', 'bold'),
        ('editbx', 'light gray', 'dark gray'),
        ('buttn', 'black', 'dark cyan'),
        ('buttnf', 'white', 'dark blue', 'bold'),
        ]

    footer_text = [
        ('title', "Example Data Browser"), "    ",
        ('key', "UP"), ",", ('key', "DOWN"), ",",
        ('key', "PAGE UP"), ",", ('key', "PAGE DOWN"),
        "  ",
        ('key', "+"), ",",
        ('key', "-"), "  ",
        ('key', "LEFT"), "  ",
        ('key', "HOME"), "  ",
        ('key', "END"), "  ",
        ('key', "F8"), " quit"
        ]

    def __init__(self):
        v = ChoiceOption('v', "", ('a', 'b', 'c', 1))
        w = ChoiceOption('w', "", ('a', 'b', 'c', 1), callback=return_value, callback_params=Params(ParamOption(v)))
        f = IntOption('f', 'if 1 must hide below', 1)
        g = IntOption('g', 'hide if 1 in previous',
                      requires=[{'option': f, 'expected': 1, 'action': 'disabled'}])
        h = IntOption('h', 'if 2 must hide below')
        i = OptionDescription('i', 'hide if 2 in previous', [StrOption('v', 'hide if 2 in previous too')],
                      requires=[{'option': h, 'expected': 2, 'action': 'disabled'}])


        config = Config(OptionDescription('root',
                                          'root',
                                          [BoolOption('a', 'boolean option'),
                                           ChoiceOption('e', 'choice option', ('oui', 'non', 'peut-être', 3)),
                                           f,
                                           g,
                                           h,
                                           i,
                                           StrOption('j', 'multi', ['1', '2', '3'], multi=True),
                                           OptionDescription('b',
                                                             '',
                                                             [IntOption('c',
                                                                        'int option'),
                                                              StrOption('d',
                                                                        '',
                                                                        'default'),
                                                              v,
                                                              w])]))
        config.property.read_write()
        self.listbox = TiramisuTui(config)
        self.listbox.offset_rows = 1
        self.header = urwid.Text('tiramisu browser')
        self.footer = urwid.AttrWrap(urwid.Text(self.footer_text),
                                     'foot')
        self.view = urwid.Frame(
            urwid.AttrWrap(self.listbox, 'body'),
            header=urwid.AttrWrap(self.header, 'head'),
            footer=self.footer)

        # Run the program
        self.loop = urwid.MainLoop(self.view,
                                   self.palette,
                                   unhandled_input=self.unhandled_input)
        self.loop.run()

    def unhandled_input(self, k):
        if k in ('f8'):
            raise urwid.ExitMainLoop()


if __name__ == "__main__":
    SimpleUI()
