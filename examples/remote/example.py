import urwid
import sys
from tiramisu_text_ui import TiramisuTui, get_palette

from urllib.request import urlopen, Request
from json import loads, dumps
from tiramisu_json_api import Config


if len(sys.argv) != 2:
    print('usage: {} [url]'.format(sys.argv[0]))
    sys.exit(1)
URL = sys.argv[1]


class AmonEcoleUI:

    def __init__(self,
                 config,
                 root):
        footer_text = [
            ('title', "Example Data Browser"), "    ",
            ('key', "UP"), ",", ('key', "DOWN"), ",",
            ('key', "PAGE UP"), ",", ('key', "PAGE DOWN"),
            "  ",
            ('key', "+"), ",",
            ('key', "-"), "  ",
            ('key', "LEFT"), "  ",
            ('key', "HOME"), "  ",
            ('key', "END"), "  ",
            ('key', "F8"), " quit"
            ]

        palette = get_palette()
        palette.extend([('body', 'black', 'light gray'),
                        ('head', 'yellow', 'black', 'standout'),
                        ('foot', 'light gray', 'black')])
        tui = TiramisuTui(config, root)
        tui.offset_rows = 1
        header = urwid.Text('tiramisu browser')
        footer = urwid.AttrWrap(urwid.Text(footer_text),
                                'foot')
        view = urwid.Frame(
            urwid.AttrWrap(tui, 'body'),
            header=urwid.AttrWrap(header, 'head'),
            footer=footer)
        tui.set_frame(view)

        # Run the program
        loop = urwid.MainLoop(view,
                              palette,
                              unhandled_input=self.unhandled_input)
        loop.run()

    def unhandled_input(self, key):
        if key in ('f8',):
            raise urwid.ExitMainLoop()


class RemoteConfig(Config):
    def __init__(self,
                 url):
        json = loads(urlopen(url).read())
        super().__init__(json)
        self.url = url

    def send_data(self,
                  updates):
        request = Request(self.url, dumps(updates).encode(), method='POST')
        return loads(urlopen(request).read())


def main():
    config = RemoteConfig(URL)
    AmonEcoleUI(config, root='creole')
    print(config.updates)


if __name__ == "__main__":
    main()
