# Copyright (C) 2018-2019 Team tiramisu (see AUTHORS for all contributors)
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from typing import Dict, List, Union, Optional, Any
import warnings
import urwid
from urwid import signals
from urwid.wimp import SelectableIcon
try:
    from tiramisu import Config
    from tiramisu.error import ValueWarning, ValueErrorWarning, PropertiesOptionError
    from tiramisu.setting import undefined
except ImportError:
    from tiramisu_json_api.error import ValueWarning, ValueErrorWarning, PropertiesOptionError
    from tiramisu_json_api.setting import undefined
    Config = None
try:
    from tiramisu_json_api import Config as ConfigJson
    if Config is None:
        Config = ConfigJson
except ImportError:
    ConfigJson = Config


if Config is None:
    raise ImportError('cannot import tiramisu nor tiramisu_json_api')


SIZE = 45
DEFAULT_PALETTE = [('flagged', 'black', 'dark green', ('bold', 'underline')),
                   ('focus', 'light gray', 'dark cyan', 'standout'),
                   ('flagged focus', 'yellow', 'dark cyan',
                    ('bold', 'standout', 'underline')),
                   ('key', 'light cyan', 'black', 'underline'),
                   ('title', 'white', 'black', 'bold'),
                   ('expandico', 'black', 'light gray', 'standout'),
                   ('expandicofc', 'black', 'dark cyan', 'bold'),
                   ('unexpandico', 'black', 'light gray', 'standout'),
                   ('unexpandicofc', 'black', 'dark cyan', 'bold'),
                   ('flag', 'dark gray', 'light gray'),
                   ('error', 'white', 'dark red'),
                   ('warning', 'white', 'brown'),
                   ('footer_error','white','dark red', 'bold'),
                   ('footer_warning','white','brown', 'bold'),
                   ('editfc', 'white', 'dark green', 'bold'),
                   ('editbx', 'light gray', 'dark gray'),
                   ('buttn', 'black', 'dark cyan'),
                   ('buttnf', 'white', 'dark blue', 'bold'),
                   ]


def get_palette():
    return DEFAULT_PALETTE


class FrozenOption(urwid.Text):
    pass


warnings.simplefilter("always", ValueWarning)


class TuiOptionDescription(urwid.TreeWidget):
    """ Display widget for leaf nodes """
    # apply an attribute to the expand/unexpand icons
    indent_cols = 1
    unexpanded_icon = urwid.AttrMap(SelectableIcon('\u229E', 1), 'expandico', 'expandicofc')
    expanded_icon = urwid.AttrMap(SelectableIcon('\u229F', 1), 'unexpandico', 'unexpandicofc')

    def __init__(self,
                 node: 'TuiParentNode') -> None:
        self.__super.__init__(node)
        # insert an extra AttrWrap for our own use
        self._w = urwid.AttrWrap(self._w, None)
        self._w.attr = 'body'
        self._w.focus_attr = 'focus'

    def load_inner_widget(self) -> urwid.Text:
        self.widget = urwid.Text(self.get_display_text())
        return self.widget

    def get_display_text(self) -> str:
        return self.get_node().get_value().option.doc()

    def change_focus(self):
        pass


class TuiOptionRoot(TuiOptionDescription):
    def __init__(self,
                 *args,
                 root_name: str=None,
                 **kwargs) -> None:
        self.root_name = root_name
        super().__init__(*args, **kwargs)

    def get_display_text(self) -> str:
        return self.root_name


class TuiOption(urwid.TreeWidget):
    """ Display widget for leaf nodes """
    signals = ['tiramisu_change',
               'tiramisu_default',
               'tiramisu_add',
               'tiramisu_del',
               'tiramisu_footer_ori',
               'tiramisu_footer_warning',
               'tiramisu_footer_error']
    indent_cols = 1

    def __init__(self,
                 node: 'TuiNode') -> None:
        self.widget = None
        self.error = None
        self.warning = None
        self.__super.__init__(node)
        # insert an extra AttrWrap for our own use
        self._w = urwid.AttrWrap(self._w, None)
        if self.error:
            self._w.attr = 'error'
            self._w.focus_attr = 'error'
        elif self.warning:
            self._w.attr = 'warning'
            self._w.focus_attr = 'warning'
        else:
            self._w.attr = 'body'
            self._w.focus_attr = 'focus'

    def selectable(self) -> bool:
        return True

    def keypress(self, size, key):
        """allow subclasses to intercept keystrokes"""
        if isinstance(self.widget, FrozenOption):
            return key
        return super(urwid.TreeWidget, self).keypress(size, key)


    def load_inner_widget(self) -> urwid.Columns:
        option = self.get_node().get_value()
        with warnings.catch_warnings(record=True) as warns:
            value = option.value.get()
        if warns:
            self.display_warns(warns, change_attr=False)
        widgets = [('pack', urwid.Text('- ' + self.get_display_text() + '   '))]
        if option.option.ismulti():
            pile_widgets = []
            if value:
                size = SIZE
            else:
                size = 0
            for idx, val in enumerate(value):
                del_button = urwid.Button('del', self.on_value_del)
                del_button.index = idx
                pile_widgets.append(urwid.Columns([('fixed', size, self._load_inner_widget(val, idx)),
                                                   del_button]))
            pile_widgets.append(urwid.Columns([('fixed', size, urwid.Text('')), urwid.Button('add', self.on_value_add)]))
            widgets.append(('fixed', size + 7, urwid.Pile(pile_widgets)))
            widgets = [urwid.Columns(widgets)]
        else:
            widgets.append(self._load_inner_widget(value, None))
        properties = option.property.get()
        if 'clearable' in properties and 'frozen' not in properties:
            widgets.append(('fixed', 11, urwid.Button('default',
                                                      self.set_default)))
        return urwid.Columns(widgets)

    def _load_inner_widget(self,
                           value: Optional[int],
                           index: Optional[int]) -> urwid.Text:
        if value is None:
            value = ''
        input_line = FrozenOption(str(value))
        input_line.index = index
        self.widget = input_line
        return self.widget

    def set_default(self,
                    button: urwid.Button) -> None:
        option = self.get_node().get_value()
        signals.emit_signal(self,
                            'tiramisu_default',
                            option.option.path())

    def get_display_text(self) -> str:
        data = self.get_node().get_value()
        return data.option.doc()

    def display_warns(self,
                      warns: List[warnings.WarningMessage],
                      change_attr: bool=True) -> None:
        if isinstance(warns[0].message, ValueErrorWarning):
            if change_attr:
                self._w.attr = 'error'
                self._w.focus_attr = 'error'
            warns[0].message.value_error.prefix = ''
            self.error = str(warns[0].message.value_error)
            self.warning = None
        else:
            if change_attr:
                self._w.attr = 'warning'
                self._w.focus_attr = 'warning'
            self.error = None
            self.warning = str(warns[0].message)

    def remove_warns(self):
        self.warning = None
        self.error = None
        self._w.attr = 'body'
        self._w.focus_attr = 'focus'

    def on_value_change(self,
                        path: str,
                        warn: List) -> None:

        if warn:
            self.display_warns(warn)
            if isinstance(warn[0].message, ValueErrorWarning):
                signals.emit_signal(self,
                                    'tiramisu_footer_error',
                                    self.error)
            else:
                signals.emit_signal(self,
                                    'tiramisu_footer_warning',
                                    self.warning)
        else:
            self._w.attr = 'body'
            self._w.focus_attr = 'focus'
            self.error = None
            self.warning = None
            signals.emit_signal(self,
                                'tiramisu_footer_ori')
        signals.emit_signal(self,
                            'tiramisu_change',
                            path)

    def on_value_add(self,
                     widget: urwid.Button) -> None:
        option = self.get_node().get_value()
        signals.emit_signal(self,
                            'tiramisu_add',
                            option.option.path())

    def on_value_del(self,
                     widget: urwid.Button) -> None:
        option = self.get_node().get_value()
        signals.emit_signal(self,
                            'tiramisu_del',
                            option.option.path(),
                            widget.index)

    def change_focus(self):
        pass


class TuiIntOption(TuiOption):
    """ Display widget for leaf nodes """

    def _load_inner_widget(self,
                           value: Optional[int],
                           index: Optional[int]) -> urwid.Padding:
        if value is None:
            value = ''
        input_line = urwid.IntEdit('',
                                   value)
        input_line.index = index
        urwid.connect_signal(input_line, 'postchange', self.on_value_change)
        self.widget = input_line
        return urwid.Padding(urwid.AttrWrap(input_line,
                                            'editbx',
                                            'editfc'),
                             width=SIZE)

    def on_value_change(self,
                        widget: urwid.IntEdit,
                        value: Union[str, int]) -> None:
        option = self.get_node().get_value()
        path = option.option.path()
        value = widget.get_edit_text()
        if value == '':
            value = None
        else:
            value = int(value)
        with warnings.catch_warnings(record=True) as warns:
            if widget.index is None:
                option.value.set(value)
            else:
                values = option.value.get()
                values[widget.index] = value
                option.value.set(values)
        super().on_value_change(path, warns)


class TuiStrOption(TuiOption):
    """ Display widget for leaf nodes """
    mask = None

    def _load_inner_widget(self, 
                           value: Optional[str],
                           index: Optional[int]) -> urwid.Padding:
        if value is None:
            value = ''
        input_line = urwid.Edit('',
                                value,
                                mask=self.mask)
        input_line.index = index
        urwid.connect_signal(input_line, 'postchange', self.on_value_change)
        self.widget = input_line
        return urwid.Padding(urwid.AttrWrap(input_line,
                                            'editbx',
                                            'editfc'),
                             width=SIZE)

    def on_value_change(self,
                        widget: urwid.Edit,
                        value: str) -> None:
        option = self.get_node().get_value()
        path = option.option.path()
        value = widget.get_edit_text()
        warns = []
        if widget.index is not None:
            with warnings.catch_warnings(record=True) as warns:
                values = option.value.get()
            values[widget.index] = value
            with warnings.catch_warnings(record=True) as warns:
                option.value.set(values)
        else:
            with warnings.catch_warnings(record=True) as warns:
                option.value.set(value)
        super().on_value_change(path, warns)


class TuiPasswordOption(TuiStrOption):
    mask = '*'


class TuiChoiceOption(TuiOption):
    """ Display widget for leaf nodes """

    def _load_inner_widget(self,
                           value: Any,
                           index: Optional[int]) -> urwid.Pile:
        option = self.get_node().get_value()
        radio_button_group = []
        overlay_widget = {}

        if 'mandatory' not in option.property.get():
            # if not mandatory, add <no value> entry (for None value)
            widget = urwid.RadioButton(radio_button_group,
                                       '<no value>',
                                       state=value is None,
                                       user_data=None,
                                       on_state_change=self.on_value_change)
            widget.index = index
            overlay_widget[None] = widget
        for txt in option.value.list():
            if txt == '':
                continue
            widget = urwid.RadioButton(radio_button_group,
                                       str(txt),
                                       state=value == txt,
                                       user_data=txt,
                                       on_state_change=self.on_value_change)
            widget.index = index
            overlay_widget[txt] = widget
        
        self.overlay_widget = urwid.Pile(overlay_widget.values())
        return self.overlay_widget
        # if value is None:
        #     return urwid.Text('<no value>')
        # return urwid.Text('(X) {}'.format(value))

    def on_value_change(self,
                        widget,
                        state: bool,
                        user_data=None) -> None:
        if state:
            option = self.get_node().get_value()
            path = option.option.path()
            with warnings.catch_warnings(record=True) as warns:
                if widget.index is not None:
                    values = option.value.get()
                    values[widget.index] = user_data
                    option.value.set(values)
                else:
                    option.value.set(user_data)
            super().on_value_change(path, warns)

    # def change_focus(self):
    #      urwid.Overlay('exit', self, 'center', None, 'middle', None)


class TuiBoolOption(TuiOption):
    """ Display widget for leaf nodes """

    def _load_inner_widget(self,
                           value: Optional[bool],
                           index: Optional[int]) -> urwid.Columns:
        if 'mandatory' in self.get_node().get_value().property.get():
            has_mixed = False
        else:
            has_mixed = True
        if value is None:
            value = 'mixed'
        self.widget = urwid.CheckBox('',
                                     state=value,
                                     has_mixed=has_mixed,
                                     on_state_change=self.on_value_change)
        self.widget.index = index
        return self.widget

    def on_value_change(self,
                        widget: urwid.CheckBox,
                        state: bool) -> None:
        option = self.get_node().get_value()
        path = option.option.path()
        if state == 'mixed':
            state = None
        with warnings.catch_warnings(record=True) as warns:
            if widget.index is not None:
                values = option.value.get()
                values[widget.index] = state
                option.value.set(values)
            else:
                option.value.set(state)
        super().on_value_change(path, warns)


class TuiCallbacks:
    def add_callbacks(self,
                      parent: 'TuiParentNode',
                      node: Union['TuiNode', 'TuiParentNode']) -> None:
        for name, callback in (('tiramisu_default', parent.on_value_default),
                               ('tiramisu_change', parent.on_value_change),
                               ('tiramisu_add', parent.on_value_add),
                               ('tiramisu_del', parent.on_value_del),
                               ('tiramisu_footer_ori', parent.on_footer),
                               ('tiramisu_footer_warning', parent.on_warning),
                               ('tiramisu_footer_error', parent.on_error)):
            urwid.connect_signal(node,
                                 name,
                                 callback)


class TuiNode(urwid.TreeNode, TuiCallbacks):
    """ Data storage object for leaf nodes """

    def __init__(self,
                 *args,
                 config: Union[Config, ConfigJson],
                 **kwargs) -> None:
        signals.register_signal(self.__class__, ['tiramisu_change',
                                                 'tiramisu_default',
                                                 'tiramisu_add',
                                                 'tiramisu_del',
                                                 'tiramisu_footer_ori',
                                                 'tiramisu_footer_warning',
                                                 'tiramisu_footer_error'])
        super().__init__(*args, **kwargs)

    def load_widget(self) -> TuiOption:
        option = self.get_value()
        if 'frozen' in option.property.get() or option.option.issymlinkoption():
            widget = TuiOption(self)
        else:
            type_ = option.option.type()
            for tiramisu_option, tui_option in [['boolean', TuiBoolOption],
                                                ['choice', TuiChoiceOption],
                                                ['integer', TuiIntOption],
                                                ['password', TuiPasswordOption],
                                                ['date', TuiStrOption],
                                                ['domain name', TuiStrOption],
                                                ['netmask address', TuiStrOption],
                                                ['port', TuiStrOption],
                                                ['file name', TuiStrOption],
                                                ['email address', TuiStrOption],
                                                ['IP', TuiStrOption],
                                                ['URL', TuiStrOption],
                                                ['username', TuiStrOption],
                                                ['string', TuiStrOption]]:
                if type_ == tiramisu_option:
                    widget = tui_option(self)
                    break
            else:
                raise NotImplementedError('{} not supported'
                                          ''.format(type_))
            self.add_callbacks(self, widget)

        return widget

    def on_value_change(self,
                        path: str) -> None:
        signals.emit_signal(self,
                            'tiramisu_change',
                            path)

    def on_value_default(self,
                         path: str) -> None:
        signals.emit_signal(self,
                            'tiramisu_default',
                            path)

    def on_value_add(self,
                     path: str) -> None:
        signals.emit_signal(self,
                            'tiramisu_add',
                            path)

    def on_value_del(self,
                     path: str,
                     index: int) -> None:
        signals.emit_signal(self,
                            'tiramisu_del',
                            path,
                            index)

    def on_error(self,
                 msg: str) -> None:
        signals.emit_signal(self,
                            'tiramisu_footer_error',
                            msg)

    def on_warning(self,
                   msg: str) -> None:
        signals.emit_signal(self,
                            'tiramisu_footer_warning',
                            msg)

    def on_footer(self) -> None:
        signals.emit_signal(self,
                            'tiramisu_footer_ori')


class TuiRootNode(urwid.ParentNode, TuiCallbacks):
    """ Data storage object for interior/parent nodes """

    def __init__(self,
                 *args,
                 config: Union[Config, ConfigJson],
                 tiramisu_tui: Optional['TiramisuTui']=None,
                 root_name: str=None,
                 **kwargs) -> None:
        signals.register_signal(self.__class__, ['tiramisu_change',
                                                 'tiramisu_default',
                                                 'tiramisu_add',
                                                 'tiramisu_del',
                                                 'tiramisu_footer_ori',
                                                 'tiramisu_footer_warning',
                                                 'tiramisu_footer_error'])
        self.config = config
        if root_name is None:
            root_name = 'root'
        self.root_name = root_name
        if tiramisu_tui is not None:
            self.tiramisu_tui = tiramisu_tui
            self.widgets = {}
        super().__init__(*args, **kwargs)

    def load_widget(self) -> TuiOptionRoot:
        return TuiOptionRoot(self, root_name=self.root_name)

    def load_child_keys(self) -> List[str]:
        data = self._load_child_keys()
        return [option.option.path() for option in data.list('all')]

    def _load_child_keys(self) -> 'Option':
        data = self.get_value()
        with warnings.catch_warnings(record=True) as warns:
            # store loaded value.dict()
            self.data = {'dict': data.value.dict(fullpath=True,
                                                 withwarning=True)}
        self.data['warns'] = [warn.message.opt().impl_getpath() for warn in warns]

        if isinstance(data, (Config, ConfigJson)):
            return data.option
        else:
            return data

    def load_child_node(self,
                        path: str) -> Union['TuiParentNode', TuiNode]:
        """Return either an TuiNode or TuiParentNode"""
        subconfig = self.config.option(path)
        if subconfig.option.isoptiondescription():
            if subconfig.option.isleadership():
                childclass = TuiLeaderNode
            else:
                childclass = TuiParentNode
        else:
            childclass = TuiNode
        widget = childclass(subconfig,
                            parent=self,
                            config=self.config,
                            key=path,
                            depth=self.get_depth() + 1)
        if subconfig.option.isoptiondescription():
            self.widgets[subconfig.option.path()] = widget
        self.add_callbacks(self, widget)
        return widget

    def duplicate_node(self,
                       path: str,
                       warns: Dict[str, List[warnings.WarningMessage]]) -> TuiNode:
        if '.' not in path:
            parent = self
        #for current_path in path.split('.')[:-1]:
        #    pass
        else:
            parent_path = path.rsplit('.', 1)[0]
            if parent_path not in self.widgets:
                return
            parent = self.widgets[parent_path]
        old_node = parent.get_child_node(path)
        # build a new TuiNode to replace old one with new value
        node = TuiNode(old_node.get_value(),
                       parent=parent,
                       config=self.config,
                       key=path,
                       depth=old_node.get_depth())
        if path in warns:
            # FIXME 0
            widget = node.get_widget()
            widget.display_warns(warns[path])
            del warns[path]
        self.add_callbacks(parent, node)
        parent.set_child_node(path, node)
        return node

    def _on_value_modified(self,
                           path: str) -> None:
        node = self.duplicate_node(path, {})
        if node:
            # change focus otherwise node is not loaded
            self.tiramisu_tui.set_focus(self)
            self.tiramisu_tui.set_focus(node)
        self.on_value_change(path)

    def on_value_add(self,
                     path: str) -> None:
        option = self.config.option(path)
        value = option.value.get()
        value.append(undefined)
        with warnings.catch_warnings(record=True) as warns:
            option.value.set(value)
        self._on_value_modified(path)

    def on_value_del(self,
                     path: str,
                     index: int) -> None:
        option = self.config.option(path)
        value = option.value.get()
        del value[index]
        with warnings.catch_warnings(record=True) as warns:
            option.value.set(value)
        self._on_value_modified(path)

    def on_value_default(self,
                         path: str) -> None:
        option = self.config.option(path)
        option.value.reset()
        self._on_value_modified(path)

    def on_value_change(self,
                        path: str) -> None:
        with warnings.catch_warnings(record=True) as warns:
            new_dict = self.get_value().value.dict(fullpath=True,
                                                   withwarning=True)
        self.update_values(path, new_dict, warns)

    def get_parent_node(self,
                        path: str,
                        old_keys: List[str]) -> 'TuiRootNode':
        if '.' not in path:
            parent_path = ''
            return self
        if '.' in path:
            splitted_path = path.split('.')
            splitted_fullpath= [splitted_path[0]]
            parent = self
            for current_path in splitted_path[1:]:
                splitted_fullpath.append(current_path)
                parent_path = '.'.join(splitted_fullpath)
                for key in old_keys:
                    if key.startswith(parent_path + '.'):
                        break
                else:
                    break
                # if parent_path is in old_keys
                try:
                    self.config.option(parent_path).group_type()
                except PropertiesOptionError:
                    break
                parent = parent.get_child_node(parent_path)
        return parent

    def update_values(self,
                      path: str,
                      new_dict: dict,
                      warns: List[warnings.WarningMessage]) -> None:
        old_warns = self.data['warns']
        old_dict = self.data['dict']
        old_keys = set(old_dict.keys())
        new_keys = set(new_dict.keys())

        keys_change = old_keys ^ new_keys
        if keys_change:
            if path not in new_keys:
                # if current is removed select root node
                self.tiramisu_tui.set_focus(self)
                self.tiramisu_tui.set_focus(self)
            parent_done = []
            for reload_path in keys_change:
                parent = self.get_parent_node(reload_path, old_keys)
                if parent in parent_done:
                    # already reloaded
                    continue
                with warnings.catch_warnings(record=True) as lwarns:
                    parent.get_child_keys(reload=True)
                parent_done.append(parent)

        warns_dict = {}
        new_warns = []
        for warn in warns:
            warn_path = warn.message.opt().impl_getpath()
            warns_dict.setdefault(warn_path, []).append(warn)
            new_warns.append(warn_path)
        for key in old_keys & new_keys:
            if path != key and old_dict[key] != new_dict[key]:
                # Config value is different than Tui's one
                self.duplicate_node(key, warns_dict)
                if key in old_warns:
                    old_warns.remove(key)
        if path in old_warns:
            old_warns.remove(path)
        # display warning or error for other option
        for key, warns in warns_dict.items():
            if '.' not in key:
                parent = self
            else:
                parent_path = key.rsplit('.', 1)[0]
                if parent_path not in self.widgets:
                    continue
                parent = self.widgets[parent_path]
            if key not in parent.get_child_keys():
                continue
            parent.get_child_node(key).get_widget().display_warns(warns)
            if key in old_warns:
                old_warns.remove(key)
        # remove old warning or error
        for key in old_warns:
            if '.' not in key:
                parent = self
            else:
                parent_path = key.rsplit('.', 1)[0]
                if parent_path not in self.widgets:
                    continue
                parent = self.widgets[parent_path]
            try:
                parent.get_child_node(key).get_widget().remove_warns()
            except PropertiesOptionError:
                pass
        self.data = {'dict': new_dict,
                     'warns': new_warns}

    def on_warning(self,
                   msg: str) -> None:
        signals.emit_signal(self,
                            'tiramisu_footer_warning',
                            msg)

    def on_error(self,
                 msg: str) -> None:
        signals.emit_signal(self,
                            'tiramisu_footer_error',
                            msg)

    def on_footer(self) -> None:
        signals.emit_signal(self,
                            'tiramisu_footer_ori')


class TuiParentNode(TuiRootNode):
    """ Data storage object for interior/parent nodes """

    def __init__(self,
                 *args,
                 config: Union[Config, ConfigJson],
                 **kwargs) -> None:
        super().__init__(*args, config=config, **kwargs)
        self.widgets = kwargs['parent'].widgets

    def _load_child_keys(self) -> None:
        return self.get_value()

    def load_widget(self) -> TuiOptionDescription:
        return TuiOptionDescription(self)

    def on_value_add(self,
                     path: str) -> None:
        signals.emit_signal(self,
                            'tiramisu_add',
                            path)

    def on_value_del(self,
                     path: str,
                     index: int) -> None:
        signals.emit_signal(self,
                            'tiramisu_del',
                            path,
                            index)

    def on_value_default(self,
                         path: str) -> None:
        signals.emit_signal(self,
                            'tiramisu_default',
                            path)

    def on_value_change(self,
                        path: str) -> None:
        signals.emit_signal(self,
                            'tiramisu_change',
                            path)

    def on_warning(self,
                 msg: str) -> None:
        signals.emit_signal(self,
                            'tiramisu_footer_warning',
                            msg)

    def on_error(self,
                 msg: str) -> None:
        signals.emit_signal(self,
                            'tiramisu_footer_error',
                            msg)

    def on_footer(self) -> None:
        signals.emit_signal(self,
                            'tiramisu_footer_ori')


class TuiLeaderNode(TuiParentNode):
    def load_child_keys(self) -> List[str]:
        # FIXME ...
        return []


class TuiTreeWalker(urwid.TreeWalker):
    """
    fake TreeWalker to resolv bug when press HOME or END key
    """
    def positions(self, reverse=False):
        if reverse:
            yield from reversed(list(self.positions()))
            return
        cur = self.focus.get_root().get_widget()
        while cur is not None:
            yield cur.get_node()
            cur = cur.next_inorder()


class TiramisuTui(urwid.TreeListBox):
    def __init__(self,
                 config: Union[Config, ConfigJson],
                 root: Optional[str]=None,
                 root_name: Optional[str]=None) -> None:
        if 'demoting_error_warning' not in config.property.get():
            raise Exception('demoting_error_warning property is mandatory for TiramisuTui')
        self.frame = None
        self.footer = None
        self.ori_footer = None
        if root is None:
            subconfig = config
        else:
            subconfig = config.option(root)
        topnode = TuiRootNode(subconfig,
                              config=config,
                              tiramisu_tui=self,
                              root_name=root_name)
        urwid.connect_signal(topnode,
                             'tiramisu_footer_error',
                             self.on_error)
        urwid.connect_signal(topnode,
                             'tiramisu_footer_warning',
                             self.on_warning)
        urwid.connect_signal(topnode,
                             'tiramisu_footer_ori',
                             self.on_footer)
        super().__init__(TuiTreeWalker(topnode))

    def set_frame(self,
                  frame: Any) -> None:
        self.frame = frame
        self.ori_footer = frame.footer

    def on_warning(self,
                   msg: str) -> None:
        if self.frame:
            self.frame.footer = urwid.AttrWrap(urwid.Text([u"Error: ", msg]), 'footer_warning')

    def on_error(self,
                 msg: str) -> None:
        if self.frame:
            self.frame.footer = urwid.AttrWrap(urwid.Text([u"Error: ", msg]), 'footer_error')

    def on_footer(self) -> None:
        if self.frame:
            self.frame.footer = self.ori_footer

    def change_focus(self,
                     *args,
                     **kwargs) -> None:
        if not isinstance(args[1], TuiRootNode):
            if args[1].get_widget().error:
                self.on_error(args[1].get_widget().error)
            elif args[1].get_widget().warning:
                self.on_warning(args[1].get_widget().warning)
            else:
                self.on_footer()
        else:
            self.on_footer()
        args[1].get_widget().change_focus()
        super().change_focus(*args, **kwargs)
